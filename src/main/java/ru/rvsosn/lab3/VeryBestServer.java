package ru.rvsosn.lab3;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class VeryBestServer {
    private Logger logger = LoggerFactory.getLogger(VeryBestServer.class);

    private final int port;
    private final String name;
    private final String parent;
    private final int parentPort;

    private Set<Socket> childrens = new HashSet<>();
    private Gson gson = new Gson();
    private Map<Socket, DataInputStream> inputStreams = new HashMap<>();
    private Map<Socket, DataOutputStream> outputStreams = new HashMap<>();
    private ExecutorService executorService = Executors.newFixedThreadPool(5);

    private Socket parentSocket;

    public VeryBestServer(int port, String name, String parent, int parentPort) {
        this.port = port;
        this.name = name;
        this.parent = parent;
        this.parentPort = parentPort;
    }

    public void start() throws InterruptedException, IOException {
        if (hasParent()) {
            parentSocket = new Socket(parent, parentPort);
            inputStreams.put(parentSocket, new DataInputStream(parentSocket.getInputStream()));
            outputStreams.put(parentSocket, new DataOutputStream(parentSocket.getOutputStream()));
            executorService.execute(() -> {
                try {
                    while (!parentSocket.isClosed()) {
                        String entry = inputStreams.get(parentSocket).readUTF();
                        logger.info("Raw incoming message {}", entry);
                        Message message = new Message(entry.split(":"));
                        logger.info("Incoming message from parent {}: {}", parentSocket.getInetAddress(), message);
                        handleMessage(parentSocket, message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

        Thread acceptConnectionsThread = new Thread(() -> {
            try (ServerSocket server = new ServerSocket(port)) {
                while (!server.isClosed() && !Thread.interrupted()) {
                    Socket client = server.accept();
                    logger.info("Accepted connection from " + client.getInetAddress());

                    childrens.add(client);
                    inputStreams.put(client, new DataInputStream(client.getInputStream()));
                    outputStreams.put(client, new DataOutputStream(client.getOutputStream()));

                    executorService.execute(() -> {
                        try {
                            while (!client.isClosed()) {
                                String entry = inputStreams.get(client).readUTF();
                                Message message = gson.fromJson(entry, Message.class);

                                logger.info("Incoming message from {}: {}", client.getInetAddress(), message);
                                handleMessage(client, message);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Thread keyboardThread = new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                Message message = new Message(scanner.next(), scanner.next());
                logger.info("Entered message " + message);
                handleMessage(null, message);
            }
        });

        keyboardThread.start();

        acceptConnectionsThread.start();
        acceptConnectionsThread.join();
    }

    private boolean hasParent() {
        return parent != null;
    }

    private void handleMessage(Socket client, Message message) {
        if (Objects.equals(message.getName(), name)) {
            logger.info("I received message {}", message);
            return;
        }

        String jsoned = gson.toJson(message);


        for (Socket childrenSockets : childrens) {
            if (childrenSockets == client) {
                continue;
            }

            try {
                logger.info("Sending message {} in json {} to {}", message, jsoned, outputStreams.get(childrenSockets));
                outputStreams.get(childrenSockets).writeUTF(jsoned);
            } catch (IOException e) {
                logger.warn("", e);

            }
        }

        if (hasParent() && client != parentSocket) {
            try {
                logger.info("Sending message {} in json {}", message, jsoned);
                outputStreams.get(parentSocket).writeUTF(jsoned);
            } catch (IOException e) {
                logger.warn("", e);
            }
        }
    }

    class Message {
        private String name;
        private String message;

        public Message(String name, String message) {
            this.name = name;
            this.message = message;
        }

        public Message(String[] splitted) {
            this.name = splitted[0];
            this.message = splitted[1];
        }

        public String getName() {
            return name;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return name + ":" + message;
        }
    }
}