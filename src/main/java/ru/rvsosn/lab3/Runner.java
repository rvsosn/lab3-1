package ru.rvsosn.lab3;

import org.slf4j.impl.SimpleLogger;

import java.io.IOException;

public class Runner {

    public static void main(String[] args) throws IOException, InterruptedException {
        System.setProperty(SimpleLogger.SHOW_DATE_TIME_KEY, "true");
        System.setProperty(SimpleLogger.DATE_TIME_FORMAT_KEY, "yyyy-MM-dd hh:mm:ss");

        int port = Integer.parseInt(args[0]);
        String name = args[1];
        String parent = null;
        int parentPort = 0;

        if (args.length > 2) {
            parent = args[2];
            parentPort = Integer.parseInt(args[3]);
        }


        new VeryBestServer(port, name, parent, parentPort).start();
    }
}
